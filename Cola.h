#pragma once
#include <stdlib.h>

//Estructura base "Nodo" para las Pilas y Colas. 
struct Nodo
{
	int dato;
	Nodo* siguiente;
	~Nodo(){};
};

public class Cola
{
private:
	Nodo* frente;
	Nodo* fin;
public:
	Cola()
	{
		frente = NULL;
		fin = NULL;
	};
	~Cola()
	{
	};
	/*
		Para incertar elementos en una cola hay tres pasos.
		Paso 1. Recervar espacio en memoria para almacenar un nodo.
		Paso 2. Asignar ese nuevo nodo al dato que queremos insertar.
		Paso 3. Asignar los punteros frente y fin hacia el nuevo nodo.
	*/
	void Push(int valor)
	{
		Nodo* nuevoNodo = new Nodo();//Paso 1.
		//Paso 2.
		nuevoNodo->dato = valor;
		nuevoNodo->siguiente = NULL;
		//Paso 3. 
		if (colaVacia(this->frente))
			this->frente = nuevoNodo;
		else
			this->fin->siguiente = nuevoNodo;

		this->fin = nuevoNodo;

	}
	//Funcion para determinar si la cola est� vac�a o no.
	bool colaVacia(Nodo* frente)
	{
		return (frente == NULL) ? true : false;
	}

	/*
		Pasos para sacar elementos de una Cola.
		1. Obtener el Valor del Nodo.
		2. Crear un Nodo aux y asignarle el frente de la cola.
		3. Eliminar el nodo del frente de la cola.

	*/
	int Pop()
	{
		int valor = this->frente->dato;//Paso 1. 
		Nodo* aux = this->frente; //Paso 2. 

		//Paso 3.
		if (this->frente == this->fin) //Si hay un solo nodo en la cola
		{
			this->frente = NULL;
			this->fin = NULL;
		}
		else
			this->frente = frente->siguiente;

		delete aux;

		return valor;
	}

	///Devuelve true si la Cola est� vac�a y false si no lo est�
	bool isEmpty()
	{
		return (this->frente == NULL) ? true : false;
	}
};