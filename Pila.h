#pragma once
#include <stdlib.h>
//Estructura base "Nodo" para las Pilas y Colas. 
struct Nodo
{
	int dato;
	Nodo* siguiente;
};

public class Pila
{
private:
	Nodo* pila;
public:
	Pila()
	{
		pila = NULL;
	};
	~Pila()
	{
	};
	/*
		Pasos para insertar elementos en una pila.
		1. Crear el espacio en memoria para almacenar un nodo.
		2. Cargar el valor dentro del nodo (dato).
		3. Cargar el puntero dentro del nodo(*siguiente).
		4. Asignar el nuevo nodo a pila.
	*/
	void Push(int valor)
	{
		Nodo* nuevoNodo = new Nodo(); //Paso 1. 
		nuevoNodo->dato = valor;//Paso 2. 
		nuevoNodo->siguiente = this->pila;//Paso3. 
		this->pila = nuevoNodo;//Paso 4. 
	}
	/*
		Pasos para sacar elementos de una pila.
		1. Crear una variable *aux de tipo Nodo.
		2. Igualar "n" a aux->dato;
		3. Pasar "pila" al siguiente nodo.
		4. Eliminar aux.
	*/
	int Pop()
	{
		Nodo* aux = this->pila;//Paso 1. 
		int valor = aux->dato;//Paso 2. 
		this->pila = aux->siguiente;//Paso3
		//por eso necesitamos una variable aux. 
	   //Para mantener una referencia para luego eliminarlo. 
		delete aux;
		return valor;
	}

	///Devuelve true si la pila est� vac�a y false si no lo est�
	bool isEmpty()
	{
		return (pila == NULL) ? true : false;
	}
};