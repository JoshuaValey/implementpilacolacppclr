#include "pch.h"
#include <stdlib.h>



using namespace System;
//using namespace std;


struct Nodo
{
	int dato;
	Nodo* siguiente;
};

public class Pila
{
private:
	Nodo* pila;
public:
	Pila()
	{
		pila = NULL;
	};
	~Pila()
	{
	};
	/*
		Pasos para insertar elementos en una pila.
		1. Crear el espacio en memoria para almacenar un nodo.
		2. Cargar el valor dentro del nodo (dato).
		3. Cargar el puntero dentro del nodo(*siguiente).
		4. Asignar el nuevo nodo a pila.
	*/
	void Push(int valor)
	{
		Nodo* nuevoNodo = new Nodo(); //Paso 1. 
		nuevoNodo->dato = valor;//Paso 2. 
		nuevoNodo->siguiente = this->pila;//Paso3. 
		this->pila = nuevoNodo;//Paso 4. 
	}
	/*
		Pasos para sacar elementos de una pila.
		1. Crear una variable *aux de tipo Nodo.
		2. Igualar "n" a aux->dato;
		3. Pasar "pila" al siguiente nodo.
		4. Eliminar aux.
	*/
	int Pop()
	{
		Nodo* aux = this->pila;//Paso 1. 
		int valor = aux->dato;//Paso 2. 
		this->pila = aux->siguiente;//Paso3
		//por eso necesitamos una variable aux. 
	   //Para mantener una referencia para luego eliminarlo. 
		delete aux;
		return valor;
	}

	///Devuelve true si la pila est� vac�a y false si no lo est�
	bool isEmpty()
	{
		return (pila == NULL) ? true : false;
	}
};
public class Cola
{
private:
	Nodo* frente;
	Nodo* fin;
public:
	Cola()
	{
		frente = NULL;
		fin = NULL;
	};
	~Cola()
	{
	};
	/*
		Para incertar elementos en una cola hay tres pasos.
		Paso 1. Recervar espacio en memoria para almacenar un nodo.
		Paso 2. Asignar ese nuevo nodo al dato que queremos insertar.
		Paso 3. Asignar los punteros frente y fin hacia el nuevo nodo.
	*/
	void Push(int valor)
	{
		Nodo* nuevoNodo = new Nodo();//Paso 1.
		//Paso 2.
		nuevoNodo->dato = valor;
		nuevoNodo->siguiente = NULL;
		//Paso 3. 
		if (colaVacia(this->frente))
			this->frente = nuevoNodo;
		else
			this->fin->siguiente = nuevoNodo;

		this->fin = nuevoNodo;

	}
	//Funcion para determinar si la cola est� vac�a o no.
	bool colaVacia(Nodo* frente)
	{
		return (frente == NULL) ? true : false;
	}

	/*
		Pasos para sacar elementos de una Cola.
		1. Obtener el Valor del Nodo.
		2. Crear un Nodo aux y asignarle el frente de la cola.
		3. Eliminar el nodo del frente de la cola.

	*/
	int Pop()
	{
		int valor = this->frente->dato;//Paso 1. 
		Nodo* aux = this->frente; //Paso 2. 

		//Paso 3.
		if (this->frente == this->fin) //Si hay un solo nodo en la cola
		{
			this->frente = NULL;
			this->fin = NULL;
		}
		else
			this->frente = frente->siguiente;

		delete aux;

		return valor;
	}

	///Devuelve true si la Cola est� vac�a y false si no lo est�
	bool isEmpty()
	{
		return (this->frente == NULL) ? true : false;
	}
};
int main(array<System::String ^> ^args)
{
	Pila* pila = new Pila();
	Cola* cola = new Cola();

	int datos[10] = {0,1,2,3,4,5,6,7,8,9};

	for (int i = 0; i < 10; i++)
	{
		pila->Push(datos[i]);
		cola->Push(datos[i]);
	}

	Console::Write("\n");
	while (!pila->isEmpty())
	{
		Console::Write(pila->Pop() + ",");
	}
	Console::Write("\n");
	while (!cola->isEmpty())
	{
		Console::Write(cola->Pop() + ",");
	}

	Console::ReadLine();

}

